//==============Internal use only Do Not modify below this line===================
#ifndef _alpha_init_
#define _alpha_init_

#if defined (PARTICLE)
#include "application.h"
#else
#include "Arduino.h"
#endif
//==============Internal use only Do Not modify above this line===================



//
//--------------Start of User Parameters--------------------------------------------------
//
// Temp and Humidity settings for Standard and Incubator modes
#define DEF_TEMP_STANDARD       90      // Standard mode temperature (degrees F)
#define DEF_HUMID_STANDARD      50      // Standard mode humidity (% RH)
#define DEF_TEMP_INCUBATOR      90      // Incubator mode temperature (degrees F)
#define DEF_HUMID_INCUBATOR     90      // Incubator mode humidity (% RH)

//Threshhold settings
#define HEATER_ON_THSHD         2       // Heater on threshhold (degrees above target)
#define HEATER_OFF_THSHD        1       // Heater off threshhold (degrees below target)

#define VENTFAN_ON_THSHD        105     // Ventilation fan on threshhold (degrees)
#define DEHUMIDIFIER_ON_THSHD   .05     // Humidifier on threshhold (percent above target)
#define DEHUMIDIFIER_OFF_THSHD  0       // Humidifier off threshhold (percent below target)
#define DEHUMIDIFIER_GUARDBAND  2       // PID Setpoint_humidifier + DEHUMIDIFIER_GUARDBAND
#define MISTER_CYCLE_ON_SECONDS 120     // Number of seconds water pump remains on when triggered
#define MISTER_CYCLE_OFF_MINUTES 58     // Number of minutes water pump remains off after completion of a cycle

//Disaster conditions
#define DISASTER_TIME_LIMIT     2       // Disaster time trigger (min)
#define DISASTER_TEMP_MIN       75      // Disaster min temp trigger (F)
#define DISASTER_TEMP_MAX       105     // Disaster max temp trigger (F)
#define DISASTER_TEMP_THSHD     10      // Disaster temp threshhold for sensor failure
#define DISASTER_HUMID_MIN      35      // Disaster min humid trigger (%)
#define DISASTER_HUMID_MAX      70      // Disaster max humid trigger (%)
#define DISASTER_HUMID_THSHD    20      // Disaster humidity threshhold for sensor failure

//Run Circulation fan X seconds after fan trigger condition deasserts
#define CIRCFAN_OFFDELAY        1800    // Circulation fan off delay (s)
#define CIRCFAN_TEMP_THSHD      2       // Circulation fan on if Temp > threshhold (degrees above target)
#define CIRCFAN_HUMID_THSHD     4       // Circulation fan on if Humidity > threshhold (% above target)

//Ventilation Fan cycle time
#define VENTFAN_CYCLE_ON        5       // Ventilation fan on (min)
#define VENTFAN_CYCLE_OFF       55      // Ventilation fan off (min)

// Legal parameter settings for Particle Function calls
#define MAX_TIME_SETTING        5760    //Cloud function max time setting (minutes)
#define MIN_TEMP_SETTING        32      //Cloud function min temp setting (degrees F)
#define MAX_TEMP_SETTING        100     //Cloud function max temp setting (degrees F)
#define MIN_HUMID_SETTING       0       //Cloud function min humidity setting (% RH)
#define MAX_HUMID_SETTING       100     //Cloud function max humidity setting (& RH)

// Particle publish period of Sensor data
#define SENSOR_PUBLISH_PERIOD   3600000 // (ms)


//--------------Start of User Harware Configuration--------------------------------------------------
char pubInfo[255];

// Option to run with relay shield or directly from Particle pins
// ATTENTION: Relay shield uses SPI on D0/D1 pins.
#define USE_RELAY_SHIELD        1       //Set 1 if using NCD8Relay shield else 0

/// Optional Sensor Pak
#define USE_SHT10_SENSOR        0       //Set 1 if Soil Moisture-Temp sensor is attached else 0

// Relay assignments. If using CE relay shield, valid values are 1-8 else use standard Arduino notation (i.e.Ax,Dy)
#if ( USE_RELAY_SHIELD )
    #define CIRCUFAN            1       // Relay for Internal Circulation Fan
    #define VENTIFAN            2       // Relay for Ventilation Fan
    #define HEATER              3       // Relay for heater
    #define HUMIDIFIER          4       // Relay for Humidifier
    #define DEHUMIDIFIER        5       // Relay for Dehumidifier
    #define WATERPUMP           6       // Relay for Water pump
#else
    #define CIRCUFAN            D3      // Output pin for Internal Circulation Fan
    #define VENTIFAN            D4      // Output pin for Ventilation Fan
    #define HEATER              D5      // Output pin for heater
    #define HUMIDIFIER          D6      // Output pin for Humidifier
    #define DEHUMIDIFIER        D7      // Output pin for Dehumidifier
    #define WATERPUMP           D2      // Output pin for Water pump
#endif

// Temperature and Humidity sensor Type and Pin assignment
#define DHTTYPE                 DHT22   // Sensor type legal values are DHT11/21/22/AM2301/AM2302
#define NUMDHT                  4       // Number of sensors installed, 1-4
#define DHTPIN1                 A0      // Input pin for temp sensor A using standard Arduino notation (i.e.Ax,Dy)
#define DHTPIN2                 A1      // Input pin for temp sensor B
#define DHTPIN3                 TX      // Input pin for temp sensor C
#define DHTPIN4                 RX      // Input pin for temp sensor D

#if ( USE_SHT10_SENSOR )
    #define sht10_dataPin           D6  // Data pin is Green wire on SHT10x sensor
    #define sht10_clockPin          D7  // Clock pin is Yellow wire on SHT10x sensor
    #define SHT10_SAMPLE_INTERVAL   60000*5 //5 minutes
#endif

//PID Aggressive and Conservative PID Coefficients
#define HEATER_PID_CON_THSHD    3       // Threshhold to switch to Aggressive settings
#define TAGGKP                  300     // Aggressive Kp
#define TAGGKI                  40      // Aggressive Ki
#define TAGGKD                  80      // Aggressive Kd
#define TCONSKP                 25      // Conservative Kp
#define TCONSKI                 25      // Conservative Ki
#define TCONSKD                 5       // Conservative Kd

#define HUMID_PID_CON_THSHD     10      // Threshhold to switch to Aggressive settings
#define HAGGKP                  200     // Aggressive Kp
#define HAGGKI                  40      // Aggressive Ki
#define HAGGKD                  50      // Aggressive Kd
#define HCONSKP                 25      // Conservative Kp
#define HCONSKI                 12      // Conservative Ki
#define HCONSKD                 10      // Conservative Kd

#define DEHUMID_PID_CON_THSHD   3       // Threshhold to switch to Aggressive settings
#define DAGGKP                  4       // Aggressive Kp
#define DAGGKI                  0.2     // Aggressive Ki
#define DAGGKD                  1       // Aggressive Kd
#define DCONSKP                 1       // Conservative Kp
#define DCONSKI                 0.05    // Conservative Ki
#define DCONSKD                 0.25    // Conservative Kd

#define VENT_PID_CON_THSHD      3       // Threshhold to switch to Aggressive settings
#define VAGGKP                  75      // Aggressive Kp
#define VAGGKI                  0.2     // Aggressive Ki
#define VAGGKD                  1       // Aggressive Kd
#define VCONSKP                 1       // Conservative Kp
#define VCONSKI                 0.05    // Conservative Ki
#define VCONSKD                 0.25    // Conservative Kd


//Time Proportional Control window size definitions
#define HEATER_WINDOWSIZE_MAX   30000   // Heater PID window (ms)
#define HEATER_WINDOWSIZE_MIN   10000   // Heater window size minimum (ms)

#define VENT_WINDOWSIZE_MAX     8000    // Ventilation PID window (ms)
#define VENT_WINDOWSIZE_MIN     1000    // Ventilation window size minimum (ms)

#define HUMID_WINDOWSIZE_MAX    60000   // Humidifier PID window (ms)
#define HUMID_WINDOWSIZE_MIN    30000   // Humidifier window size minimum (ms)

#define DEHUMID_WINDOWSIZE_MAX  120000  // Dehumidifier PID window (ms)
#define DEHUMID_WINDOWSIZE_MIN  60000   // Dehumidifier window size minimum (ms)

//
//--------------End of User Parameter/Confinguration--------------------------------------------------
//














































//==============Internal use only Do Not modify below this line===================
#define USE_SERIAL_DEBUG        0       // Set for serial debug
#define USE_BLYNK               0       // Set to include Blynk support
#define ALLOW_RELAY_PUBLISH     1       // Set to allow Particle Publish of relay state transitions
#define ALLOW_SENSOR_PUBLISH    1       // Set to allow Particle Publish of sensor
#define DHT_SAMPLE_INTERVAL     6000    // Temperature Sample rate (mS) > 2ms

//#define sensorArray_size (60000/DHT_SAMPLE_INTERVAL*2)  //(60000ms/sample_period(ms))*minutes_of_datalog

#if( USE_SERIAL_DEBUG  && (PLATFORM_ID <= 10)) // Core, Photon, P1
    #define DBG(_x)	((void)(_x))
    #if(0)
      #define DBG_GEN(_x)	((void)(_x))
    #else
      #define DBG_GEN(_x)	((void)0)
    #endif
    #if(1)
      #define DBG_CLD(_x)	((void)(_x))
    #else
      #define DBG_CLD(_x)	((void)0)
    #endif
    #if(1)
      #define DBG_PID(_x)	((void)(_x))
      unsigned long lastpid_humid=0;
      unsigned long lastpid_dehumid=0;
      unsigned long lastpid_heater=0;
    #else
      #define DBG_PID(_x)	((void)0)
    #endif
    #if(0)
      #define DBG_RELAY(_x)	((void)(_x))
    #else
      #define DBG_RELAY(_x)	((void)0)
    #endif
    #if(1)
      #define DBG_DHT(_x)	((void)(_x))
    #else
      #define DBG_DHT(_x)	((void)0)
    #endif
    #if(0)
      #define DBG_CIRCF(_x)	((void)(_x))
    #else
      #define DBG_CIRCF(_x)	((void)0)
    #endif
    #if(0)
      #define DBG_WATERP(_x)	((void)(_x))
    #else
      #define DBG_WATERP(_x)	((void)0)
    #endif
    #if(1)
      #define DBG_SHT(_x)	((void)(_x))
    #else
      #define DBG_SHT(_x)	((void)0)
    #endif
    #if(1)
      #define DBG_PID_HEATER(_x)	((void)(_x))
    #else
      #define DBG_PID_HEATER(_x)	((void)0)
    #endif
    #if(1)
      #define DBG_PID_HUMID(_x)	((void)(_x))
    #else
      #define DBG_PID_HUMID(_x)	((void)0)
    #endif
    #if(0)
      #define DBG_PID_DEHU(_x)	((void)(_x))
    #else
      #define DBG_PID_DEHU(_x)	((void)0)
    #endif
#else
	#define DBG(_x)	((void)0)
  #define DBG_GEN(_x)	((void)0)
  #define DBG_CLD(_x)	((void)0)
  #define DBG_PID(_x)	((void)0)
  #define DBG_RELAY(_x)	((void)0)
  #define DBG_DHT(_x)	((void)0)
  #define DBG_CIRCF(_x)	((void)0)
  #define DBG_WATERP(_x)	((void)0)
  #define DBG_SHT(_x)	((void)0)
  #define DBG_PID_HEATER(_x)	((void)0)
  #define DBG_PID_HUMID(_x)	((void)0)
  #define DBG_PID_DEHU(_x)	((void)0)
#endif

#if ( USE_RELAY_SHIELD )
    #define USE_RELAY_SHIELD_RESET 1
    #include "NCD8Relay.h"
    NCD8Relay relayController;
    int relay_status_prev=0;
    int relay_status_current=0;
    bool pendPublishRelays=false;          //Flag to publish relay state

    void reset_relay_i2c(){
        if (!relayController.initialized) {
            Wire.reset();
            relayController.setAddress(0,0,0);
            }
    }
    void relay_station(int relay, int state){
        int bit_index = relay-1;
        int bit_mask = ~(0x1<<bit_index) & 0xff;

        if (state) relay_status_current = relay_status_prev | (0x1<<bit_index); //set the bit
        else relay_status_current = (relay_status_prev & (0xFFFE<<bit_index)) | (relay_status_prev & bit_mask);      //clr the bit

        if (relay_status_current != relay_status_prev){
            DBG_RELAY(Serial.printlnf("Prev Relay: %x Current Relay: %x req: %d,%d",relay_status_prev, relay_status_current, relay, state));
            relay_status_prev = relay_status_current;
            pendPublishRelays=true;
            relayController.setBankStatus(relay_status_current);    // Only post relay command if there is a state change
        }
    }
#else
    #define reset_relay_i2c(_x) ((void)0)
    volatile bool pendPublishRelays=false;          //Flag to publish relay state
    void relayChangeISR(){
        pendPublishRelays=true;
    }
#endif

#if( USE_BLYNK )
    #define BLNK(_x)	          ((void)(_x))
    #define CIRCUFAN_ON()       led0.on()
    #define VENTIFAN_ON()       led1.on()
    #define HEATER_ON()         led2.on()
    #define HUMIDIFIER_ON()     led3.on()
    #define DEHUMIDIFIER_ON()   led4.on()
    #define CIRCUFAN_OFF()      led0.off()
    #define VENTIFAN_OFF()      led1.off()
    #define HEATER_OFF()        led2.off()
    #define HUMIDIFIER_OFF()    led3.off()
    #define DEHUMIDIFIER_OFF()  led4.off()
    #define readRelay(_x)       digitalRead(_x)
#else
	#define BLNK(_x)	            ((void)0)
  #if ( USE_RELAY_SHIELD )
      #define CIRCUFAN_ON()       do{relay_station(CIRCUFAN, 1);} while (0)
      #define WATERPUMP_ON()      do{relay_station(WATERPUMP, 1);} while (0)
      #define VENTIFAN_ON()       do{relay_station(VENTIFAN, 1);ventfan_on=true;} while (0)
      #define HEATER_ON()         do{relay_station(HEATER, 1);heater_on=true;} while (0)
      #define HUMIDIFIER_ON()     do{relay_station(HUMIDIFIER, 1);humidifier_on=true;} while (0)
      #define DEHUMIDIFIER_ON()   do{relay_station(DEHUMIDIFIER, 1);dehumidifier_on=true;} while (0)
      #define CIRCUFAN_OFF()      do{relay_station(CIRCUFAN, 0);} while (0)
      #define WATERPUMP_OFF()     do{relay_station(WATERPUMP, 0);} while (0)
      #define VENTIFAN_OFF()      do{relay_station(VENTIFAN, 0);ventfan_on=false;} while (0)
      #define HEATER_OFF()        do{relay_station(HEATER, 0);heater_on=false;} while (0)
      #define HUMIDIFIER_OFF()    do{relay_station(HUMIDIFIER, 0);humidifier_on=false;} while (0)
      #define DEHUMIDIFIER_OFF()  do{relay_station(DEHUMIDIFIER, 0);dehumidifier_on=false;} while (0)
      #define readRelay(_x)       (relay_status_current & (0x1<<(_x-1)))>0
  #else
      #define CIRCUFAN_ON()       do{digitalWrite(CIRCUFAN, HIGH);} while (0)
      #define WATERPUMP_ON()      do{digitalWrite(WATERPUMP, HIGH);} while (0)
      #define VENTIFAN_ON()       do{digitalWrite(VENTIFAN, HIGH);ventfan_on=true;} while (0)
      #define HEATER_ON()         do{digitalWrite(HEATER, HIGH);heater_on=true;} while (0)
      #define HUMIDIFIER_ON()     do{digitalWrite(HUMIDIFIER, HIGH);humidifier_on=true;} while (0)
      #define DEHUMIDIFIER_ON()   do{digitalWrite(DEHUMIDIFIER, HIGH);dehumidifier_on=true;} while (0)
      #define CIRCUFAN_OFF()      do{digitalWrite(CIRCUFAN, LOW);} while (0)
      #define WATERPUMP_OFF()     do{digitalWrite(WATERPUMP, LOW);} while (0)
      #define VENTIFAN_OFF()      do{digitalWrite(VENTIFAN, LOW);ventfan_on=false;} while (0)
      #define HEATER_OFF()        do{digitalWrite(HEATER, LOW);heater_on=false;} while (0)
      #define HUMIDIFIER_OFF()    do{digitalWrite(HUMIDIFIER, LOW);humidifier_on=false;} while (0)
      #define DEHUMIDIFIER_OFF()  do{digitalWrite(DEHUMIDIFIER, LOW);dehumidifier_on=false;} while (0)
      #define readRelay(_x)       digitalRead(_x)
  #endif
#endif

#if (NUMDHT==1)
  int DHTPIN[] = {DHTPIN1};
  #define do_DHT_aquire(_x) do{dht_result[0]=dht_inst[0]->acquireAndWait(_x);} while (0)
#elif (NUMDHT==2)
  int DHTPIN[] = {DHTPIN1,DHTPIN2};
  #define do_DHT_aquire(_x) do{dht_result[0]=dht_inst[0]->acquire();dht_result[1]=dht_inst[1]->acquireAndWait(_x);} while(0)
#elif (NUMDHT==3)
  int DHTPIN[] = {DHTPIN1,DHTPIN2,DHTPIN3};
  #define do_DHT_aquire(_x) do{dht_result[0]=dht_inst[0]->acquire();dht_result[1]=dht_inst[1]->acquire();dht_result[2]=dht_inst[2]->acquireAndWait(_x);} while(0)
#elif (NUMDHT==4)
  int DHTPIN[] = {DHTPIN1,DHTPIN2,DHTPIN3,DHTPIN4};
  #define do_DHT_aquire(_x) do{dht_result[0]=dht_inst[0]->acquire();dht_result[1]=dht_inst[1]->acquire();dht_result[2]=dht_inst[2]->acquire();dht_result[3]=dht_inst[3]->acquireAndWait(_x);} while(0)
#endif


#endif
