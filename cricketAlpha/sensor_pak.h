#ifndef _sensor_pak_
#define _sensor_pak_

#if defined (PARTICLE)
#include "application.h"
#else
#include "Arduino.h"
#endif


#if ( USE_SHT10_SENSOR )
#include "Sensirion.h"

Sensirion sht(sht10_dataPin, sht10_clockPin);
unsigned long last_time_in_sht10_loop=0;
byte measActive = false;
byte measType = TEMP;
uint16_t rawData_sht10;
float temperature_sht10;

void sensor_sht10()
{
    float humidity;
    float temperatureF;

    // Read values from the sensor
    if (millis()-last_time_in_sht10_loop >SHT10_SAMPLE_INTERVAL) {
        last_time_in_sht10_loop = millis();
        measActive = true;
        measType = TEMP;
        sht.meas(TEMP, &rawData_sht10, NONBLOCK);             // Start temp measurement
        DBG_SHT(Serial.println("SHT10 start sample."));
      }
    if (measActive && sht.measRdy()) {                        // Check measurement status
        if (measType == TEMP) {                               // Process temp or humi?
            measType = HUMI;
            temperature_sht10 = sht.calcTemp(rawData_sht10);  // Convert raw sensor data
            temperatureF = sht.calcTempF(rawData_sht10);      // Convert raw sensor data
            sht.meas(HUMI, &rawData_sht10, NONBLOCK);         // Start humi measurement
            DBG_SHT(Serial.printlnf("SHT10 tempC:%.2f tempF:%.2f",temperature_sht10, temperatureF));
        } else {
            measActive = false;
            humidity = sht.calcHumi(rawData_sht10, temperature_sht10); // Convert raw sensor data
            sprintf(pubInfo, "{\"SHT10\":[{\"Sensor\":\"Humidity\",\"Value\":[\"%.2f\"]}]}", humidity);
            Particle.publish("reportSensorsPak", pubInfo, 60, PRIVATE);
            DBG_SHT(Serial.printlnf("SHT10 Moisture:%.2f%%",humidity));
            }
    }
}
#else
  #define sensor_sht10(_x)  ((void)0)
#endif
#endif
