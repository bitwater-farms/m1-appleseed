#ifndef _cloud_functions_
#define _cloud_functions_

#if defined (PARTICLE)
#include "application.h"
#else
#include "Arduino.h"
#endif

#define MODE_STANDARD 0
#define MODE_INCUBATOR 1
#define CMD_ACTION_ON 1
#define CMD_ACTION_OFF 0

uint16_t sub_cmd[2];                            //Parse function call returning action and time
int user_temp_setting;
bool override_temp=false;
unsigned long override_humid_stop_time;
unsigned long override_vent_stop_time;
unsigned long override_circ_stop_time;
unsigned long override_temp_stop_time;

bool override_humid=false;
bool override_vent=false;
bool override_circ=false;

int user_humid_setting;
int user_vent_setting;
int user_circ_setting;

int sys_mode = MODE_STANDARD;                   // Operating mode is standard or incubator
int baseline_temp_setting = DEF_TEMP_STANDARD;  // Defaults to Stardard mode settings
int baseline_humid_setting= DEF_HUMID_STANDARD; // Defaults to Stardard mode settings


bool initPublish = true;
unsigned long _lastTimeSensorPublish=0;
char relay_variable[140];
char sensor_variable[255];

void doPublishRelays(){
    sprintf(pubInfo, "{\"Circulation\":\"%d\",\"Ventilation\":\"%d\",\"Heater\":\"%d\",\"Humidifier\":\"%d\",\"Dehumidifier\":\"%d\",\"Waterpump\":\"%d\"}",
                      readRelay(CIRCUFAN),readRelay(VENTIFAN),readRelay(HEATER),readRelay(HUMIDIFIER),readRelay(DEHUMIDIFIER),readRelay(WATERPUMP)
                      );
    sprintf(relay_variable,"%s",pubInfo);
    #if (ALLOW_RELAY_PUBLISH)
      Particle.publish("reportControls", pubInfo, 60, PRIVATE);
    #endif
    pendPublishRelays=false;
    initPublish=false;
}

void doPublishSensors(){
      sprintf(pubInfo, "{\"am2302\":[{\"Sensor\":\"Temp\",\"Value\":[\"%.2f\",\"%.2f\",\"%.2f\",\"%.2f\"]},{\"Sensor\":\"Humidity\",\"Value\":[\"%.2f\",\"%.2f\",\"%.2f\",\"%.2f\"]}]}",
                        current_temp[0],current_temp[1],current_temp[2],current_temp[3],current_humid[0], current_humid[1], current_humid[2], current_humid[3]
                        );
    sprintf(sensor_variable,"%s",pubInfo);
    for (int x=0;x<NUMDHT;x++){                                                       // In the mean time, save off Current Sample to Last Sample
      last_temp_published[x] = current_temp[x];
      last_humid_published[x] = current_humid[x];
    }
    #if (ALLOW_SENSOR_PUBLISH)
      Particle.publish("reportSensors", pubInfo, 60, PRIVATE);
    #endif
    _lastTimeSensorPublish = millis();
}

int func_setMode(String command) {
  if (command=="standard") {
        sys_mode=MODE_STANDARD;
        baseline_temp_setting = DEF_TEMP_STANDARD;
        baseline_humid_setting = DEF_TEMP_STANDARD;
        return 1;
        }
  else if (command=="incubator"){
        sys_mode=MODE_INCUBATOR;
        baseline_temp_setting = DEF_TEMP_INCUBATOR;
        baseline_humid_setting = DEF_TEMP_INCUBATOR;
        return 1;
        }
  else return -1;
}

//Parse Particle Function for the proper syntax <action><delimiter>[parameter]
//Input: Command String
//Returns:  true if function is correctly formatted
//          false if function is malformed
//sub_cmd[0] holds the action, sub_cmd[1] action parameter
//
bool parse_command(String command) {
    int delimiter = command.indexOf(":");
    String action;
    if (delimiter>0) {
        action = command.substring(0,delimiter);
        if (action.equalsIgnoreCase("on")) sub_cmd[0] = CMD_ACTION_ON;
        else if (action.equalsIgnoreCase("off")) sub_cmd[0] = CMD_ACTION_OFF;
        else sub_cmd[0] = action.toInt();
        sub_cmd[1] = command.substring(delimiter+1).toInt();
        return true;
    }
    else return false;
}

//Particle Function setTemp service routine
//Returns:   1 if successful
//          -1 if unsuccessful
int func_setTemperature(String command) {
  if (parse_command(command)) { //Temperature:minutes
    if (sub_cmd[1] <= 0){
        override_temp = false;
        DBG_CLD(Serial.println("Cloud function: setTemp: OFF"));
    }
    else if((sub_cmd[1] >  0) &&
       (sub_cmd[1] <= MAX_TIME_SETTING) &&
       (sub_cmd[0] >  MIN_TEMP_SETTING) &&
       (sub_cmd[0] <= MAX_TEMP_SETTING)) { // Check for valid settings
            user_temp_setting = sub_cmd[0];
            override_temp = true;
            override_temp_stop_time = millis()+60000*sub_cmd[1];
            DBG_CLD(Serial.printlnf("Cloud function: setTemp %i:%i",sub_cmd[0],sub_cmd[1]));
            return 1;
        }
    else return -1; //do nothing
    }
  else return -1; //do nothing
}

//Particle Function service routine
int func_setHumidity(String command) {
  if (parse_command(command)) { //Humidity:minutes
    if (sub_cmd[1] <= 0){
        override_humid = false;
        DBG_CLD(Serial.println("Cloud function: setHumidity: OFF"));
    }
    else if((sub_cmd[1] >  0) &&
       (sub_cmd[1] <= MAX_TIME_SETTING) &&
       (sub_cmd[0] >  MIN_HUMID_SETTING) &&
       (sub_cmd[0] <= MAX_HUMID_SETTING)) { // Check for valid settings
            user_humid_setting = sub_cmd[0];
            override_humid = true;
            override_humid_stop_time = millis()+60000*sub_cmd[1];
            DBG_CLD(Serial.printlnf("Cloud function: setHumidity %i:%i:%i",user_humid_setting,sub_cmd[1],override_humid));
            return 1;
        }
    else return -1; //do nothing
    }
  else return -1; //do nothing
}

//Particle Function service routine
int func_setVentilation(String command) {
  if (parse_command(command)) { //on|off:minutes
    switch (sub_cmd[0]) {
        case CMD_ACTION_ON:  if((sub_cmd[1] > 0) && (sub_cmd[1] <= MAX_TIME_SETTING)) { // Check if time is valid
                                user_vent_setting = sub_cmd[0];
                                override_vent = true;
                                override_vent_stop_time = millis()+60000*sub_cmd[1];
                                DBG_CLD(Serial.printlnf("Cloud function: setVentilation ON:%i",sub_cmd[1]));
                                return 1;
                                }
                            else return 0; break; //do nothing
        case CMD_ACTION_OFF: override_vent = false;
                             DBG_CLD(Serial.printlnf("Cloud function: setVentilation OFF:%i",sub_cmd[1]));
                             return 1; break;
    }
  }
  else return -1;
}

//Particle Function service routine
int func_setCirculation(String command) {
  if (parse_command(command)) { //on|off:minutes
    switch (sub_cmd[0]) {
        case CMD_ACTION_ON:  if((sub_cmd[1] > 0) && (sub_cmd[1] <= MAX_TIME_SETTING)) { // Check if time is valid
                                override_circ = true;
                                override_circ_stop_time = millis()+60000*sub_cmd[1];
                                DBG_CLD(Serial.printlnf("Cloud function: setCirculation ON:%i",sub_cmd[1]));
                                return 1;
                                }
                            else return 0;break;
        case CMD_ACTION_OFF: override_circ = false;
                             DBG_CLD(Serial.printlnf("Cloud function: setCirculation OFF:%i",sub_cmd[1]));
                             return 1; break;
    }
  }
  else return -1;
}

//Hold overrride values for the duration specified by the respective override function parameter
void evaluate_ovrds(){
    unsigned long clock_in=millis();
    override_humid = override_humid && (clock_in < override_humid_stop_time);
    override_vent  = override_vent  && (clock_in < override_vent_stop_time);
    override_circ  = override_circ  && (clock_in < override_circ_stop_time);
    override_temp  = override_temp  && (clock_in < override_temp_stop_time);
}
#endif
