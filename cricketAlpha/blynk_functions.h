#ifndef _blynk_functions_
#define _blynk_functions_
#if( USE_BLYNK )
char auth[] = "3e94ce9764dc46f791c64a0d552bbc2c";
WidgetLED led0(V0); //register to virtual pin 0
WidgetLED led1(V1); //register to virtual pin 1
WidgetLED led2(V2); //register to virtual pin 2
WidgetLED led3(V3); //register to virtual pin 3
WidgetLED led4(V4); //register to virtual pin 4
WidgetTerminal terminal(V5);

IntervalTimer blynkTimer;

volatile bool pendBlynkPub=false;

void blynk_publish_graph(float Input_temp, float Input_humid) {
    Blynk.virtualWrite(V10,Input_temp);
    Blynk.virtualWrite(V11,Input_humid);
    Blynk.virtualWrite(V12,digitalRead(VENTIFAN)*50);
    Blynk.virtualWrite(V13,digitalRead(HEATER)*50);
    pendBlynkPub=false;
}

void blynkTimerISR(){
    pendBlynkPub=true;
}
#else
bool pendBlynkPub=false;
#endif
#endif
