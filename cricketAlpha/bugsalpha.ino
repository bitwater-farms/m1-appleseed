// This #include statement was automatically added by the Particle IDE.
#include "bugsalpha_init.h"

// This #include statement was automatically added by the Particle IDE.
#include "blynk.h"

// This #include statement was automatically added by the Particle IDE.
#include "PietteTech_DHT.h"

// This #include statement was automatically added by the Particle IDE.
#include "SparkIntervalTimer.h"

// This #include statement was automatically added by the Particle IDE.
#include "pid.h"

// This #include statement was automatically added by the Particle IDE.
#include "dht_functions.h"

// This #include statement was automatically added by the Particle IDE.
#include "blynk_functions.h"

// This #include statement was automatically added by the Particle IDE.
#include "Sensirion.h"

// This #include statement was automatically added by the Particle IDE.
#include "sensor_pak.h"

// This #include statement was automatically added by the Particle IDE.
#include "cloud_functions.h"

//SYSTEM_THREAD(ENABLED);

unsigned long lastTimeInLoop=0, lasttimeprinted=0, lasttime_waterpump_off, circfan_on_delaytimer=0, waterpump_on_delaytimer=0;


//Define Variables we'll be connecting to
double Setpoint_temp=100, Input_temp=0, Output_heater, Output_vent;
double Setpoint_humid=100, Input_humid=0, Output_humid, Output_dehumid;
double sys_setpoint_temp, sys_setpoint_humid, sys_setpoint_dehumid;
float temp_delta;
float humid_delta;
float temp_error=0;
float humid_error=0;
struct pairs minmax_temp;
struct pairs minmax_humid;

bool circfan_on=false;
bool circfan_in_prev =false;
bool circfan_in=false;
bool ventfan_req=false;
bool ventfan_on=false;
bool heater_req=false;
bool heater_on=false;
bool humidifier_req=false;
bool humidifier_on=false;
bool dehumidifier_req=false;
bool dehumidifier_on=false;
bool windowOnVentfan=false;
bool waterpump_on=false;
bool waterpump_on_prev=false;
bool waterpump_qual=false;
bool waterpump_in=false;
bool waterpump_in_prev=false;
bool minmax_temp_exceeded;
bool minmax_humid_exceeded;
bool heater_disaster_pend=false;
bool vent_disaster_pend=false;
bool humid_disaster_pend=false;
bool dehumid_disaster_pend=false;
unsigned long heater_disaster_timer_start;
unsigned long vent_disaster_timer_start;
unsigned long humid_disaster_timer_start;
unsigned long dehumid_disaster_timer_start;

int WindowSizeVentFan = (VENTFAN_CYCLE_ON + VENTFAN_CYCLE_OFF)*60000; //(ms)

unsigned long windowStartVentFan=0;
unsigned long windowStartTimeTemp=0;
unsigned long windowStartTimeHumid=0;
unsigned long windowStartTimeDehumid=0;


//Specify the links and initial tuning parameters
PID tempPID(&Input_temp, &Output_heater, &sys_setpoint_temp, TCONSKP, TCONSKI, TCONSKD, PID::DIRECT);
PID humidPID(&Input_humid, &Output_humid, &sys_setpoint_humid, HCONSKP, HCONSKI, HCONSKD, PID::DIRECT);
PID ventPID(&Input_temp, &Output_vent, &sys_setpoint_temp, VCONSKP, VCONSKI, VCONSKD, PID::REVERSE);
PID dehumidPID(&Input_humid, &Output_dehumid, &sys_setpoint_dehumid, DCONSKP, DCONSKI, DCONSKD, PID::REVERSE);

ApplicationWatchdog wd(60000, System.reset);

void setup()
{
    waitUntil(Particle.connected);
        Particle.function("setMode",    func_setMode);            //Mode values are "standard" or "incubator"
        Particle.function("setTemp",    func_setTemperature);     //Temperature:minutes
        Particle.function("setHumidity",func_setHumidity);        //Humidity:minutes
        Particle.function("setVenti",   func_setVentilation);     //on|off:minutes
        Particle.function("setCircu",   func_setCirculation);     //on|off:minutes
        Particle.variable("relay_state", relay_variable);         //relay status
        Particle.variable("sensor_dht", sensor_variable);         //DHT sensors
        BLNK(Blynk.begin(auth));
        BLNK(blynkTimer.begin(blynkTimerISR,6000,hmSec));
        BLNK(blynkTimer.interrupt_SIT(INT_ENABLE));

    DBG(Serial.begin(115200));

    #if ( USE_RELAY_SHIELD )
        relayController.setAddress(0,0,0);                        // If CE Relay Shield installed, initialize I2C link
    #else
        pinMode(CIRCUFAN,     OUTPUT);                            // Else initialize Particle GPIO as outputs
        pinMode(VENTIFAN,     OUTPUT);
        pinMode(HEATER,       OUTPUT);
        pinMode(HUMIDIFIER,   OUTPUT);
        pinMode(DEHUMIDIFIER, OUTPUT);
        pinMode(WATERPUMP,    OUTPUT);
        attachInterrupt(CIRCUFAN,     relayChangeISR, CHANGE);    // Attach interrupt service to publish relay state change
        attachInterrupt(VENTIFAN,     relayChangeISR, CHANGE);    // Attach interrupt service to publish relay state change
        attachInterrupt(HEATER,       relayChangeISR, CHANGE);    // Attach interrupt service to publish relay state change
        attachInterrupt(HUMIDIFIER,   relayChangeISR, CHANGE);    // Attach interrupt service to publish relay state change
        attachInterrupt(DEHUMIDIFIER, relayChangeISR, CHANGE);    // Attach interrupt service to publish relay state change
        attachInterrupt(WATERPUMP,    relayChangeISR, CHANGE);    // Attach interrupt service to publish relay state change
    #endif
    for (int x=0;x<NUMDHT;x++){                                   // Initialize DHT sensor readings
      dht_inst[x]     = new PietteTech_DHT(DHTPIN[x], DHTTYPE, dht_wrapper[x]);
      current_temp[x] =0;
      current_humid[x]=0;
      last_temp[x]    =0;
      last_humid[x]   =0;
    }
    delay(5000);                                                  // Delay 5s to let the sensors settle
    //Take initial DHT sensor readings
    check_dht_init_data();

    for (int x=0;x<NUMDHT;x++){                                   // Average DHT sensor readings
      Input_temp  += current_temp[x];
      Input_humid += current_humid[x];
    }
    Input_temp  = Input_temp/NUMDHT;
    Input_humid = Input_humid/NUMDHT;

    sys_setpoint_temp= baseline_temp_setting;                     // Set baseline as initial PID setPoint
    sys_setpoint_humid= baseline_humid_setting;                   // Set baseline as initial PID setPoint
    sys_setpoint_dehumid= baseline_humid_setting;                 // Set baseline as initial PID setPoint

    DBG(Serial.printlnf("Initial Temperature: %.2f degrees F and Relative Humidity: %.2f%%",Input_temp, Input_humid));

    tempPID.SetMode(PID::AUTOMATIC);                              // Setup PID engine
    humidPID.SetMode(PID::AUTOMATIC);                             // Setup PID engine
    ventPID.SetMode(PID::AUTOMATIC);                              // Setup PID engine
    dehumidPID.SetMode(PID::AUTOMATIC);                           // Setup PID engine
    tempPID.SetOutputLimits   (HEATER_WINDOWSIZE_MIN,             // Setup PID Output min/max values
                                HEATER_WINDOWSIZE_MAX);
    humidPID.SetOutputLimits  (HUMID_WINDOWSIZE_MIN,              // Setup PID Output min/max values
                                HUMID_WINDOWSIZE_MAX);
    dehumidPID.SetOutputLimits(DEHUMID_WINDOWSIZE_MIN,            // Setup PID Output min/max values
                                DEHUMID_WINDOWSIZE_MAX);
    ventPID.SetOutputLimits   (VENT_WINDOWSIZE_MIN,               // Setup PID Output min/max values
                                VENT_WINDOWSIZE_MAX);
    #if ( USE_SHT10_SENSOR )                                      // Process SHT sensors if enabled
        sht.meas(TEMP, &rawData_sht10, NONBLOCK);
    #endif
    lastTimeInLoop=0;
    lasttimeprinted=0;
    lasttime_waterpump_off=millis();
}

void loop()
{
    BLNK(Blynk.run());

    float current_avg_temp=0;
    float current_avg_humid=0;
    unsigned long _ms = millis();                                                         // Save loop entry time here and use for any timers that don't
                                                                                          //   require cycle precision.

    waterpump_qual = ((_ms-lasttime_waterpump_off)>(MISTER_CYCLE_OFF_MINUTES*60000)) &&
                      !(waterpump_on||waterpump_on_prev);                                 // Check if waterpump is cleared to re-arm
    waterpump_in= (humid_delta>0) && waterpump_qual;
    if (waterpump_in && (!waterpump_in_prev)) {                                           // Create delayed shutoff for waterpump
        waterpump_on_delaytimer = _ms+(MISTER_CYCLE_ON_SECONDS*1000);
    }
    if ((!waterpump_on) && waterpump_on_prev) {
      lasttime_waterpump_off= millis();
    }
    waterpump_in_prev = waterpump_in;                                                     // Save state for next time around
    waterpump_on_prev = waterpump_on;                                                     // Save state for next time around

    if (_ms - windowStartVentFan > WindowSizeVentFan) {                                   // Generate Ventilation window
        windowStartVentFan += WindowSizeVentFan;
        }
    windowOnVentfan= (_ms - windowStartVentFan) < (VENTFAN_CYCLE_ON*60000);

    if (_ms - lastTimeInLoop > DHT_SAMPLE_INTERVAL) {                                     // Time to take new DHT measurements
        lastTimeInLoop = _ms;                                                             // Save loop entry time
        if (check_delta_sensors()) doPublishSensors();                                    // Check delta of previous and current sample
        do_DHT_aquire(0);                                                                 // Initiate sample request
        for (int x=0;x<NUMDHT;x++){                                                       // In the mean time, save off Current Sample to Last Sample
          last_temp[x] = current_temp[x];
          last_humid[x] = current_humid[x];
        }
        reset_relay_i2c();                                                                // Periodically check I2C connection to relay board and reset if needed
      }

    check_dht_data();                                                                     // Check if sample is ready
    int dht_error_cnt=0;
    minmax_temp = getMinMax (current_temp, NUMDHT);                                       // Find min/max of current sample
    minmax_humid = getMinMax (current_humid, NUMDHT);                                     // Find min/max of current sample
    minmax_temp_exceeded= abs(minmax_temp.max - minmax_temp.min)  > CIRCFAN_TEMP_THSHD;
    minmax_humid_exceeded= abs(minmax_humid.max - minmax_humid.min) > CIRCFAN_HUMID_THSHD;
    current_avg_temp  = minmax_temp.avrg;
    current_avg_humid = minmax_humid.avrg;
    Input_temp = 0.0*Input_temp + 1*current_avg_temp;                                     // Smooth out sensor readings
    Input_humid = 0.0*Input_humid + 1*current_avg_humid;

    check_disaster_limits();                                                              // Check Disaster limits

    //=======----- Keep this ordering for proper operation ----------
    circfan_in= minmax_temp_exceeded || minmax_humid_exceeded;
    if (circfan_in_prev && (!circfan_in)) {                                               // Create delayed shutoff for circulation fan
        circfan_on_delaytimer = millis()+(CIRCFAN_OFFDELAY*1000);
        BLNK(terminal.printlnf("circ_dtimer %i",circfan_on_delaytimer));
        BLNK(terminal.flush());
    }
    circfan_in_prev = circfan_in;                                                         // Save state for next time around
    //=======--------------------------------------------------------

    #if(USE_SERIAL_DEBUG)
    if (_ms-lasttimeprinted>DHT_SAMPLE_INTERVAL) {
      lasttimeprinted=_ms;
      DBG_DHT(Serial.printlnf("Avg %.2fF/%.2f%% :: In %.2fF/%.2f%% :: Sp %.2fF/%.2f%% :: Ovd: t%i,h%i",
              Input_temp, Input_humid, current_avg_temp, current_avg_humid,sys_setpoint_temp,
              sys_setpoint_humid, override_temp, override_humid));
      }
    #endif

    #if( USE_RELAY_SHIELD )
        relay_status_prev = relay_status_current;                                         // Save previous relay state
    #endif
    evaluate_rules(&circfan_on_delaytimer, &waterpump_on_delaytimer);                     // Evaluate rules based on current sample

    if ((millis() - _lastTimeSensorPublish) > SENSOR_PUBLISH_PERIOD) doPublishSensors();  // Periodic publish of sensor data
    if (pendPublishRelays || initPublish) doPublishRelays();                              // Service publish of relay state requests
    if (pendBlynkPub) BLNK(blynk_publish_graph(Input_temp, Input_humid));

    sys_setpoint_temp= override_temp ? user_temp_setting : baseline_temp_setting;         // Mux in setPoint based on user override
    sys_setpoint_humid= override_humid ? user_humid_setting : baseline_humid_setting;     // Mux in setPoint based on user override
    sys_setpoint_dehumid= sys_setpoint_humid+DEHUMIDIFIER_GUARDBAND;                      // Guardband dehumidifier
    temp_delta = sys_setpoint_temp-Input_temp;                                            // Calculate difference of current sample from setPoint
    humid_delta = sys_setpoint_humid-Input_humid;                                         // Calculate difference of current sample from setPoint
    temp_error = abs(temp_delta);                                                         // Calculate magnitude of error
    humid_error = abs(humid_delta);                                                       // Calculate magnitude of error

    // Determine which tuning parameters to use and process PID adjustment
    if(temp_error<HEATER_PID_CON_THSHD) tempPID.SetTunings(TCONSKP, TCONSKI, TCONSKD);    // If close to setPoint, use conservative tuning parameters
    else tempPID.SetTunings(TAGGKP, TAGGKI, TAGGKD);                                      // else far from setPoint, use aggressive tuning parameters
    if(humid_error<HUMID_PID_CON_THSHD) humidPID.SetTunings(HCONSKP, HCONSKI, HCONSKD);   // If close to setPoint, use conservative tuning parameters
    else humidPID.SetTunings(HAGGKP, HAGGKI, HAGGKD);                                     // else far from setPoint, use aggressive tuning parameters
    if(humid_error<DEHUMID_PID_CON_THSHD) dehumidPID.SetTunings(DCONSKP,DCONSKI,DCONSKD); // If close to setPoint, use conservative tuning parameters
    else dehumidPID.SetTunings(DAGGKP, DAGGKI, DAGGKD);                                   // else far from setPoint, use aggressive tuning parameters
    tempPID.Compute();
    humidPID.Compute();
    dehumidPID.Compute();

    evaluate_ovrds();                                                                     // Check Particle function override timers
    heater_TPC();                                                                         // Process Time Proportional Control using current PID output
    humidifier_TPC();                                                                     // Process Time Proportional Control using current PID output
    dehumidifier_TPC();                                                                   // Process Time Proportional Control using current PID output

    sensor_sht10();                                                                       // Process SHT sensors if enabled
}

//=======----- Spec Disaster Check -----=======//
void check_disaster_limits(){
  unsigned long _ms = millis();
  bool temp_sensor_failure_min_b = abs(minmax_temp.min-minmax_temp.min2)<DISASTER_TEMP_THSHD;
  bool humid_sensor_failure_min_b = abs(minmax_humid.min-minmax_humid.min2)<DISASTER_HUMID_THSHD;
  bool temp_sensor_failure_max_b = abs(minmax_temp.max-minmax_temp.max2)<DISASTER_TEMP_THSHD;
  bool humid_sensor_failure_max_b = abs(minmax_humid.max-minmax_humid.max2)<DISASTER_HUMID_THSHD;

  if ((minmax_temp.min<DISASTER_TEMP_MIN) && temp_sensor_failure_min_b){
    heater_disaster_pend=true;
    heater_disaster_timer_start=_ms;
  }
  else heater_disaster_pend=false;

  if ((minmax_temp.max>DISASTER_TEMP_MAX) && temp_sensor_failure_max_b){
    vent_disaster_pend=true;
    vent_disaster_timer_start=_ms;
  }
  else vent_disaster_pend=false;

  if ((minmax_humid.min<DISASTER_HUMID_MIN) && humid_sensor_failure_min_b){
    humid_disaster_pend=true;
    humid_disaster_timer_start=_ms;
  }
  else humid_disaster_pend=false;

  if ((minmax_humid.max>DISASTER_HUMID_MAX) && humid_sensor_failure_max_b){
    dehumid_disaster_pend=true;
    dehumid_disaster_timer_start=_ms;
  }
  else dehumid_disaster_pend=false;
}

//=======----- Spec Rules -----=======//
//=======----- Keep this ordering for proper operation ----------
void ventfan_rules(){    // Ventilation Fan
    bool ventfan_set = override_vent || windowOnVentfan || (Input_temp  > VENTFAN_ON_THSHD);
    bool ventfan_clr = 1;
    ventfan_req = (ventfan_req & (!ventfan_clr)) || ventfan_set;
    BLNK(terminal.printlnf("ventf st%i:cl%i:on%i sp:%.2f in:%.2f td:%.2f",ventfan_set,ventfan_clr,ventfan_req,sys_setpoint_temp,Input_temp,temp_delta));
    if (ventfan_req) VENTIFAN_ON();
    else VENTIFAN_OFF();
}
void humidifier_rules(){    // Humidifier
    humidifier_req = (humid_disaster_pend && (millis()-humid_disaster_timer_start) >DISASTER_TIME_LIMIT);
    DBG_PID_HUMID({if(millis()-lastpid_humid>DHT_SAMPLE_INTERVAL) {lastpid_humid=millis();
      Serial.printlnf("Humid  on%i %.2f %.2f %.2f",humidifier_req,sys_setpoint_humid,Output_humid,Input_humid);}});
}
void dehumidifier_rules(){    // Dehumidifier
    dehumidifier_req = (dehumid_disaster_pend && (millis()-dehumid_disaster_timer_start) >DISASTER_TIME_LIMIT);
    DBG_PID_DEHU({if(millis()-lastpid_dehumid>DHT_SAMPLE_INTERVAL) {lastpid_dehumid=millis();
      Serial.printlnf("deHu   on%i %.2f %.2f %.2f",dehumidifier_req,sys_setpoint_humid,Output_dehumid,Input_humid);}});
}
void heater_rules(){    // heater
    heater_req = (heater_disaster_pend && (millis()-heater_disaster_timer_start) >DISASTER_TIME_LIMIT);
    DBG_PID_HEATER({if(millis()-lastpid_heater>DHT_SAMPLE_INTERVAL) {lastpid_heater=millis();Serial.printlnf("heater on%i %.2f %.2f %.2f",
      heater_req,sys_setpoint_temp,Output_heater,Input_temp);}});
}
void circfan_rules(unsigned long *marktime){    // Internal Circulation Fan
    bool circfan_on_delay= millis() < *marktime;
    DBG_CIRCF(Serial.printlnf("circf ov%i:mmt:%i:mmh:%i:circd:%i",override_circ,minmax_temp_exceeded,minmax_humid_exceeded,circfan_on_delay));
    circfan_on = override_circ || circfan_in || circfan_on_delay;
    if (circfan_on) CIRCUFAN_ON();
    else CIRCUFAN_OFF();
}
void waterpump_rules(unsigned long *marktime){
    bool waterpump_on_delay= millis() < *marktime;
    DBG_WATERP(Serial.printlnf("he:%i:waterpumpd:%i",humid_delta,waterpump_on_delay));
    waterpump_on = waterpump_in || waterpump_on_delay;
    if (waterpump_on) WATERPUMP_ON();
    else WATERPUMP_OFF();
}
void evaluate_rules(unsigned long *dtime1, unsigned long *dtime2){
    ventfan_rules();
    humidifier_rules();
    dehumidifier_rules();
    heater_rules();
    circfan_rules(dtime1);
    waterpump_rules(dtime2);
    BLNK(terminal.flush());
}
//=======--------------------------------------------------------

//Time Proportional Control (TPC) functions for relay driven PID processes
void heater_TPC(){    // heater
    if (millis() - windowStartTimeTemp > HEATER_WINDOWSIZE_MAX) {
        windowStartTimeTemp += HEATER_WINDOWSIZE_MAX;
        }
    if (heater_req || ((Output_heater > HEATER_WINDOWSIZE_MIN) && (Output_heater > millis() - windowStartTimeTemp))) HEATER_ON();
    else HEATER_OFF();
}
void humidifier_TPC(){    // Humidifier
    if (millis() - windowStartTimeHumid > HUMID_WINDOWSIZE_MAX) {
        windowStartTimeHumid += HUMID_WINDOWSIZE_MAX;
        }
    if (humidifier_req || ((Output_humid > HUMID_WINDOWSIZE_MIN) && (Output_humid > millis() - windowStartTimeHumid))) HUMIDIFIER_ON();
    else HUMIDIFIER_OFF();
}
/* Temporarily disabled.... use windowing to control VENT cycles
void ventTimePC(){    // Ventilation Fan
    if (millis() - windowStartTimeVent > VENT_WINDOWSIZE_MAX) {
        windowStartTimeVent += VENT_WINDOWSIZE_MAX;
        }
    if (humidifier_req || (Output_vent > millis() - windowStartTimeVent)) VENTIFAN_ON();
    else VENTIFAN_OFF();
}
*/
void dehumidifier_TPC(){    // Dehumidifier
    if (millis() - windowStartTimeDehumid > DEHUMID_WINDOWSIZE_MAX) {
        windowStartTimeDehumid += DEHUMID_WINDOWSIZE_MAX;
        }
    if (dehumidifier_req || ((Output_dehumid > DEHUMID_WINDOWSIZE_MIN) && (Output_dehumid > millis() - windowStartTimeDehumid))) DEHUMIDIFIER_ON();
    else DEHUMIDIFIER_OFF();
}
