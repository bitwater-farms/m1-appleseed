#ifndef _dht_functions_
#define _dht_functions_

#if defined (PARTICLE)
#include "application.h"
#else
#include "Arduino.h"
#endif

//declaration
int _sensor_error_count[NUMDHT];
int dht_result[NUMDHT];
float current_temp[4];
float current_humid[4];
float last_temp[NUMDHT];
float last_humid[NUMDHT];
float last_temp_published[NUMDHT];
float last_humid_published[NUMDHT];

/*
typedef struct rec
	{
    		float T1;
    		float T2;
    		float T3;
    		float T4;
    		float H1;
    		float H2;
    		float H3;
    		float H4;
	}SENSOR_DATA;

SENSOR_DATA *sensorarray;
int sensor_ptr=0;
*/
struct pairs
{
  float min;  // min
  float max;  // max
  float min2; // 2nd min
  float max2; // 2nd max
  float avrg; // Average non-failing values
};

struct pairs getMinMax(float arr[], int n)
{
  struct pairs minmax;
  float c_arr[10];

  /*If there is only one element then return it as min and max both*/
  if (n == 1)
  {
     minmax.max = arr[0];
     minmax.min = arr[0];
     minmax.max2 = minmax.max;
     minmax.min2 = minmax.max;
     minmax.avrg = arr[0];
     return minmax;
  }
  else if (n == 2)
  {
     if (arr[0]>arr[1]){
       minmax.max = arr[0];
       minmax.min = arr[1]>0 ? arr[1] : arr[0];
     }
     else{
       minmax.max = arr[1];
       minmax.min = arr[0]>0 ? arr[0] : arr[1];
     }
     minmax.max2 = minmax.max;
     minmax.min2 = minmax.min;
     if (arr[0]==0) minmax.avrg = arr[1];
     else if (arr[1]=0) minmax.avrg = arr[0];
     else minmax.avrg = (arr[0]+arr[1])/2;
     return minmax;
  }
  else { // full bubble sort it!
    int i=0; minmax.avrg=0;
    for (int x=0;x<n;x++) c_arr[x]=arr[x];
    for(int x=0; x<n; x++){
  		for(int y=0; y<n-1; y++){
  			if(c_arr[y]>c_arr[y+1]){
  				int temp = c_arr[y+1];
  				c_arr[y+1] = c_arr[y];
  				c_arr[y] = temp;
  			}
  		}
      if (c_arr[x]>0) {
        i +=1;
        minmax.avrg += c_arr[x];
      }
  	}
    if (i) minmax.avrg /= i;
    minmax.max = c_arr[n-1];
    minmax.min = c_arr[0];
    minmax.max2 = c_arr[n-2];
    minmax.min2 = c_arr[1];
  }
  return minmax;
}

bool check_delta_sensors(){
	bool delta_sensors=false;
	for (int i=0;i<NUMDHT;i++){
		delta_sensors = delta_sensors || (abs(current_temp[i]-last_temp_published[i])>=1.0);
		delta_sensors = delta_sensors || (abs(current_humid[i]-last_humid_published[i])>=1.0);
	}
	return delta_sensors;
}

// Lib instantiate
bool active_wrapper;  // NOTE: Do not remove this, it prevents compiler from optimizing
void dht_wrapperA();  // must be declared before the lib initialization
void dht_wrapperB();  // must be declared before the lib initialization
void dht_wrapperC();  // must be declared before the lib initialization
void dht_wrapperD();  // must be declared before the lib initialization

void (*dht_wrapper[])() = {dht_wrapperA,dht_wrapperB,dht_wrapperC,dht_wrapperD};
PietteTech_DHT* dht_inst[NUMDHT];

// This wrapper is in charge of calling
// must be defined like this for the lib work
void dht_wrapperA() {
    active_wrapper = true;      // NOTE: Do not remove this, it prevents compiler from optimizing
    dht_inst[0]->isrCallback();
}

// This wrapper is in charge of calling
// must be defined like this for the lib work
void dht_wrapperB() {
    active_wrapper = false;      // NOTE: Do not remove this, it prevents compiler from optimizing
    dht_inst[1]->isrCallback();
}

// This wrapper is in charge of calling
// must be defined like this for the lib work
void dht_wrapperC() {
    active_wrapper = false;      // NOTE: Do not remove this, it prevents compiler from optimizing
    dht_inst[2]->isrCallback();
}

// This wrapper is in charge of calling
// must be defined like this for the lib work
void dht_wrapperD() {
    active_wrapper = false;      // NOTE: Do not remove this, it prevents compiler from optimizing
    dht_inst[3]->isrCallback();
}

void getSensorData(class PietteTech_DHT *_d, int sensor) {
    int result = _d->getStatus();

			//DBG_DHT(Serial.println("OK"));
			current_temp[sensor] = _d->getFahrenheit();
			current_humid[sensor] = _d->getHumidity();
      if ((current_temp[sensor] == 0) && (current_humid==0)) {
        _d->reset();
      }
}

void check_dht_init_data(){
  DBG_DHT(Serial.println("Retrieving information from sensor "));
	for (int sensor=0;sensor<NUMDHT;sensor++){
    for(int x=0;x<5;x++){
      int y=0;
      dht_result[sensor]=dht_inst[sensor]->acquireAndWait(3000);
      if (dht_result[sensor] == DHTLIB_OK) break;
      else if (dht_result[sensor]<1){
        DBG_DHT(Serial.printlnf("Sensor Error %i.%i: %i",sensor,x,dht_result[sensor]));
        delay(10);y++;
        if (y>2) break;
      }
    }
    if (dht_result[sensor] == DHTLIB_OK){
      //DBG_DHT(Serial.printlnf("Read sensor %i: ",sensor));
      getSensorData(dht_inst[sensor], sensor);
    }
    else {
      current_temp[sensor] = 0;
      current_humid[sensor] = 0;
    }
	}
}
void check_dht_data(){
	for (int sensor=0;sensor<NUMDHT;sensor++){
    dht_result[sensor]=dht_inst[sensor]->getStatus();
    if (dht_result[sensor] == DHTLIB_OK){    //reading is ready... retrieve it
      //DBG_DHT(Serial.printlnf("Retrieving information from sensor %i: ",sensor));
      getSensorData(dht_inst[sensor], sensor);
    }
    else {    //reading error... return 0 values
      DBG_DHT(Serial.printlnf("Sensor Error %i: %i",sensor,dht_result[sensor]));
      current_temp[sensor] = 0;
      current_humid[sensor] = 0;
      dht_inst[sensor]->reset();
    }
	}
}

#endif
