# Overview
Reducing the carbon footprint of humanity is now the responsibility of every world citizen. We all must do our part, while acknowledging that what each citizen can do varies wildly.

At Bitwater Farms, we partner with farmers and researchers to assist the efforts many are already making to make agriculture more sustainable. The Bitwater Farms M1 is a package for small farmers and entomologists to quickly start cricket production - either for harvest or research. Of course, there is no need to limit the use to crickets! It is a great environment to grow many types of insects.

Because we are global citizens, and recognize the undeniable effects of atmospheric carbon, the [OpenCricketFarm.com](http://www.opencricketfarm.com/) project is being released to the public.

This is the trailing version of our technology and does not integrate machine learning and other forward looking features. WE KNOW THERE MAY BE SERIOUS BUGS and yet are intentionally releasing early for the common good. Hopefully, we will release often.

*All software is provided under the [GPLv3 License](https://www.gnu.org/licenses/gpl-3.0.en.html).*

# Project Objective
The Bitwater Farms M1 uses a fairly standard [grow tent](https://www.amazon.com/gp/product/B01FIJEAAI) to create an effective cricket habitat. The bitbox hardware is built with off the shelf components and can easily be assembled by a motivated Maker. Of course, completed units are also available from Bitwater Farms. The Bitbox controls a heater, humidifier, dehumidifier, ventilation fan, circulation fan and water mister by continually monitoring four temperature and humidity sensors, to maintain an ideal environment. Additionally, using the Cloud API provided by Particle.io, remote monitoring tools can be created, as well as changing the temperature and humidity set points without being on site.

The goal of the M1 is to enable research and small scale production to change the supply chain for poultry feed, and to create a new source for the rapidly growing market of insect protein flour for human consumption. By changing poultry feed to crickets, the significant environmental effects of other food sources can be significantly reduced.

# Hardware
The Bitwater Farms Appleseed version Bitbox is built around either a Photon or Electron available at https://www.particle.io/. From there, the main components are:
* A [relay board](https://www.controleverything.com/content/Relay-Controller?sku=MCP23008_PER820) from Control Everything to handle switching of 15AMP devices.
* Four [DHT2302 DHT](https://www.adafruit.com/product/393) sensors.
* An [SHT10 soil moisture sensor](https://www.adafruit.com/product/1298).
See CricketAlpha1_Proto_02_schematic.pdf in the schematics folder for wiring details.

# Software
The software reads the temperature and humidity information in a loop and uses a PID controller to adjust the environment based on those settings. The initial setpoints, relay configuration, and PID tuning coefficients are all set in bugsalpha_init.h. The initial PID values have worked well in test environments, but may well need adjusting for specific installations.

Whenever the temperature or humidity change by more than 1 degree or 1 percent, the values are written to the Particle Cloud using *reportSensors*. This reports JSON encoded data for all the DHT sensors, which can then be stored in the Cloud for additional analysis.

Whenever the status relays change state, the current state of all relays is reported using *reportControls*.

The environment can also be adjusted by external control systems using the Cloud API. These functions are supported:
* Function “setMode”. Parameter: Mode. Mode can be “standard” or “incubator”
* Function “setTemperature”, Parameter: “Temperature:minutes”
* Function “setHumidity”, Parameter: “Humidity:minutes”
* Function “setVentilation”, Parameter “on|off:minutes”
* Function “setCirculation”, Parameter “on|off:minutes”

Standard and Incubator modes are just shortcuts to set the default temperature and humidity setpoints.
